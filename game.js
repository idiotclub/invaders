var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var gameTicksSinceLastShot = 0;
var turretFacing = 0;
var bullets = [];
var enemies = [];
var miniturrets = [];
var del = null;
var gameTicksSinceLastSpawn = -30;
var gameTicks = 0;
var del2 = null;
var spawnDelay = 40;
var paused = false;
var shop = 0;
var dough = 10;
var upg1 = 0;
var upg2 = 0;
var upg3 = 0;
var upg4 = 0;
var upg5 = 0;
var shot = 0;
var pierce_owned = false;
var fire_owned = false;
var mini_owned = false;
var mm_owned = false;
var laser_owned = false;
var musicPlaying = false;
var enemy_type = 0;
var score = 0;
var place = 0;
var clickx;
var clicky;
var upgs = 1;
var minits = 1;
var mmguns = 1;
var audio_loop = 1410;
var ene1 = Math.round(getRndNum(2, 3));
var ene2 = Math.round(getRndNum(4, 6));
var shots = 0;

var radians = Math.PI / 180;

ctx.scale(2, 2) // Draw canvas at 2x so it doesn't look terrible on retina displays
function drawRotatedImage(image, x, y, angle)
{

	// save the current co-ordinate system
	// before we screw with it
	ctx.save();

	// move to the middle of where we want to draw our image
	ctx.translate(x, y);

	// rotate around that point, converting our
	// angle from degrees to radians
	ctx.rotate(angle);

	// draw it up and to the left by half the width
	// and height of the image
	ctx.drawImage(image, -(image.width / 2), -(image.height / 2));

	// and restore the co-ords to how they were when we began
	ctx.restore();
}

function preloadImage(url)
{ // preload images by url
	var img = new Image();
	img.src = url;
}
preloadImage("upg1.png");
preloadImage("upg2.png");
preloadImage("upg3.png");
preloadImage("upg4.png");
preloadImage("upgrades.png");
preloadImage("bullets.png");
preloadImage("fireballs.png");
preloadImage("fireballso.png");
preloadImage("lasers.png");
preloadImage("laserso.png");
preloadImage("miniguns.png");
preloadImage("minigunso.png");
preloadImage("miniminigun_s.png");
preloadImage("miniturret_s.png");
preloadImage("miniturrets.png");
preloadImage("mmissiles.png");
preloadImage("mmissileso.png");
preloadImage("pierces.png");
preloadImage("pierceso.png");
preloadImage("bullet.png");

function getDistance(x, x2, y, y2)
{
	var diffX = x - x2;
	var diffY = y - y2;
	return Math.sqrt((diffX * diffX) + (diffY * diffY))
}

function circle(x, y)
{
	ctx.beginPath();
	ctx.arc(x, y, 12, 0, Math.PI * 2, false);
	ctx.fill();
}

function scircle(x, y)
{
	ctx.beginPath();
	ctx.arc(x, y, 6, 0, Math.PI * 2, false);
	ctx.fill();
}

var Bullet = function (dir, x, y, type, ticks, mini)
{
	if (!mini)
	{
		shots++;
	}
	this.mini = mini;
	this.x = x;
	this.y = y;
	this.dir = dir;
	this.type = type;
	this.place_in_array = bullets.length;
	this.ticksUntilDeath = -1;
	if (type == 3)
	{
		this.size = 12;
	}
	else if (type != 5)
	{
		this.size = 24;
	}
	else
	{
		this.size = 24 + upg4 * 4;
	}
	if (ticks < 30 - upg2 * 3 + upg4 * 4 || (ticks < 20 - upg2 * 3 + upg4 * 4 && shot != 3))
	{
		this.dir += (Math.random() - 0.5) / 2
	}
}

var Enemy = function (x, y, type)
{
	this.x = x;
	this.y = y;
	this.dir = Math.atan2(this.y - 625, this.x - 625) + ((Math.PI / 2) * 3);
	this.type = type;
	this.place_in_array = enemies.length;
	this.ticksUntilDeath = -1;
	this.invincibility = 5;
	if (type == 2)
	{
		this.step = 0;
		this.dir += Math.PI / 4;
	}
	else if (type == 3 || gameTicks >= 14000)
	{
		this.hp = 2;
		if (this.type == 3)
		{
			this.hp += 2;
		}
		if (gameTicks >= 20000)
		{
			this.hp++;
		}
	}
}

var MiniTurret = function (x, y, type)
{
	this.x = x;
	this.y = y;
	this.dir = 0;
	this.type = type;
	this.reload = 20;
	this.memory = 0;
}




Bullet.prototype.tick = function ()
{
	if (this.type == 2 || this.type == 5)
	{
		this.x += Math.sin(this.dir) * -(12 + upg1 * 2);
		this.y -= Math.cos(this.dir) * -(12 + upg1 * 2);
	}
	else if (this.type != 4)
	{
		this.x += Math.sin(this.dir) * -(5 + upg1 * 2);
		this.y -= Math.cos(this.dir) * -(5 + upg1 * 2);
	}
	else if (this.type == 4)
	{
		this.dir = Math.atan2(clicky - this.y, clickx - this.x);
		this.x += Math.sin(this.dir) * -(10 + upg1 * 2 + upg4 * 4);
		this.y -= Math.cos(this.dir) * -(10 + upg1 * 2 + upg4 * 4);
	}
	if (this.type == 0)
	{
		var img22 = new Image();
		img22.src = "bullet.png";
		drawRotatedImage(img22, this.x, this.y, this.dir + Math.PI);
	}
	else if (this.type == 1)
	{
		var img11 = new Image();
		img11.src = "pierce.png";
		drawRotatedImage(img11, this.x, this.y, this.dir + Math.PI);
	}
	else if (this.type == 2)
	{
		var img12 = new Image();
		img12.src = "fireball.png";
		drawRotatedImage(img12, this.x, this.y, this.dir + Math.PI);
	}
	else if (this.type == 3)
	{
		scircle(this.x - 3, this.y - 3);
	}
	else if (this.type == 4)
	{
		var img19 = new Image();
		img19.src = "mmissile.png";
		drawRotatedImage(img19, this.x, this.y, this.dir + Math.PI);
	}
	else
	{
		var img21 = new Image();
		img21.src = "laser.png";
		drawRotatedImage(img21, this.x, this.y, this.dir + Math.PI);
	}
	if (this.x > 1250 || this.x < 0 || this.y > 850 || this.y < 0)
	{
		del = this.place_in_array;
	}
	for (j = 0; j < enemies.length; j++)
	{
		if (enemies[j] != undefined)
		{
			if (getDistance(this.x, enemies[j].x, this.y, enemies[j].y) < this.size)
			{
				if (this.type == 0 || this.type == 3)
				{
					del = this.place_in_array;
				}
				if (enemies[j].type != 3 || gameTicks >= 14000)
				{
					dough += 3 + enemies[j].type + upg5;
					score += 1 + enemies[j].type;
					enemies[j] = undefined;
					if (this.type == 2)
					{
						this.size += 6 * (upg4 + 1);
					}
				}
				else if (enemies[j].hp == 1)
				{
					dough += 3 + enemies[j].type + upg5;
					score += 1 + enemies[j].type;
					enemies[j] = undefined;
					if (this.type == 2)
					{
						this.size += 8 * (upg4 + 1);
					}
				}
				else if (enemies[j].invincibility < 1)
				{
					enemies[j].hp--;
					enemies[j].invincibility = 20;
				}
			}
		}
	}
}
Enemy.prototype.tick = function ()
{
	if (this.type == 0 || this.type == 3)
	{
		this.x += Math.sin(this.dir) * 1;
		this.y -= Math.cos(this.dir) * 1;
	}
	else
	{
		this.x += Math.sin(this.dir) * 2;
		this.y -= Math.cos(this.dir) * 2;
	}
	if (this.type == 2)
	{
		this.step += 1;
		if (this.step == 50)
		{
			this.dir -= Math.PI / 2;
		}
		else if (this.step == 100)
		{
			this.dir += Math.PI / 2;
			this.step = 0;
		}
	}
	this.invincibility--;
	var img1 = new Image();
	if (this.type == 0)
	{
		img1.src = "red%20enemy.png";
	}
	else if (this.type == 1)
	{
		img1.src = "blue%20enemy.png";
	}
	else if (this.type == 2)
	{
		img1.src = "pink%20enemy.png";
	}
	else
	{
		img1.src = "orang%20enemy.png";
	}
	drawRotatedImage(img1, this.x, this.y, this.dir + Math.PI);
	if (this.x > 600 && this.x < 650 && this.y > 600 && this.y < 650)
	{
		clearInterval(game_loop);
		ctx.font = "96px Arial Black"
		ctx.fillStyle = "red";
		ctx.fillText("YOU LOSE.", 400, 400);
		ctx.font = "24px Arial Black"
		ctx.fillText("Score: " + score, 400, 500);
	}
}

MiniTurret.prototype.tick = function ()
{
	this.reload--;
	this.memory--;
	var img15 = new Image();
	if (this.type == 1)
	{
		img15.src = "miniturret.png"
	}
	else
	{
		img15.src = "miniminigun.png"
	}
	drawRotatedImage(img15, this.x, this.y, this.dir + (Math.PI / 2));
	if (this.reload <= 0 && this.memory > 0)
	{
		this.dir = Math.atan2(clicky - this.y, clickx - this.x);
		if (this.type == 2)
		{
			this.reload = 30;
		}
		else
		{
			this.reload = 60;
		}
		if (this.type == 1)
		{
			bullets.push(new Bullet(this.dir - Math.PI / 2, this.x, this.y, 0, 100));
		}
		else
		{
			bullets.push(new Bullet(this.dir - Math.PI / 2 + (Math.random() - 0.5) / (upg3 / 2 + 0.5), this.x, this.y, 3, 100));
			bullets.push(new Bullet(this.dir - Math.PI / 2 + (Math.random() - 0.5) / (upg3 / 2 + 0.5), this.x, this.y, 3, 100));
			bullets.push(new Bullet(this.dir - Math.PI / 2 + (Math.random() - 0.5) / (upg3 / 2 + 0.5), this.x, this.y, 3, 100));

		}
	}
}

MiniTurret.prototype.draw = function ()
{
	var img15 = new Image();
	if (this.type == 1)
	{
		img15.src = "miniturret.png"
	}
	else
	{
		img15.src = "miniminigun.png"
	}
	drawRotatedImage(img15, this.x, this.y, this.dir + (Math.PI / 2));
}

var game_loop = setInterval(function ()
{
	ctx.clearRect(0, 0, 1250, 850);
	ctx.strokeRect(0, 0, 1250, 850);
	var img2 = new Image();
	img2.src = "invaders%20turret.png";
	drawRotatedImage(img2, 625, 625, turretFacing + (Math.PI / 2));
	var shopButton = new Image();
	shopButton.src = "shop.png";
	drawRotatedImage(shopButton, 1200, 770);
	if (!paused)
	{
		gameTicksSinceLastShot++;
		gameTicks++;
		gameTicksSinceLastSpawn++;
		spawnDelay = 40 - Math.floor(gameTicks / 100);
		if (gameTicks > 2400)
		{
			spawnDelay += 20;
			enemy_type = 1;
		}
		if (gameTicks > 6000)
		{
			spawnDelay += 10;
			enemy_type = ene1;
		}
		/*if (gameTicks > 12000) {
		    enemy_type = ene2;
		}*/
		if (spawnDelay < 6)
		{
			spawnDelay = 6;
		}
		if (gameTicksSinceLastSpawn >= spawnDelay)
		{
			enemies.push(new Enemy(Math.ceil(Math.random() * 1250), 1, enemy_type));
			gameTicksSinceLastSpawn = 0;
		}
		for (i = 0; i < bullets.length; i++)
		{
			if (bullets[i] != undefined)
			{
				bullets[i].tick();
			}
		}
		for (i = 0; i < enemies.length; i++)
		{
			if (enemies[i] != undefined)
			{
				enemies[i].tick();
			}
		}
		for (i = 0; i < miniturrets.length; i++)
		{
			miniturrets[i].tick();
		}
	}
	else if (shop == 1)
	{
		for (i = 0; i < miniturrets.length; i++)
		{
			miniturrets[i].draw();
		}
		var img3 = new Image();
		img3.src = "bullets.png";
		drawRotatedImage(img3, 100, 750);
		var img4 = new Image();
		img4.src = "miniturrets.png";
		drawRotatedImage(img4, 300, 750);
		var img5 = new Image();
		img5.src = "upgrades.png";
		drawRotatedImage(img5, 500, 750);
	}
	else if (shop == 4)
	{
		for (i = 0; i < miniturrets.length; i++)
		{
			miniturrets[i].draw();
		}
		var img6 = new Image();
		img6.src = "upg1.png";
		drawRotatedImage(img6, 100, 750);
		ctx.font = "24px Arial";
		ctx.fillText("$" + 100 * upgs, 100, 820);
		var img7 = new Image();
		img7.src = "upg2.png";
		drawRotatedImage(img7, 300, 750);
		ctx.font = "24px Arial";
		ctx.fillText("$" + 100 * upgs, 300, 820);
		var img8 = new Image();
		img8.src = "upg3.png";
		drawRotatedImage(img8, 500, 750);
		ctx.font = "24px Arial";
		ctx.fillText("$" + 100 * upgs, 500, 820);
		var img9 = new Image();
		img9.src = "upg4.png";
		drawRotatedImage(img9, 700, 750);
		ctx.font = "24px Arial";
		ctx.fillText("$" + 100 * upgs, 700, 820);
	}
	else if (shop == 2)
	{
		for (i = 0; i < miniturrets.length; i++)
		{
			miniturrets[i].draw();
		}
		var img10 = new Image();
		if (pierce_owned)
		{
			img10.src = "pierceso.png"
		}
		else
		{
			img10.src = "pierces.png"
		}
		drawRotatedImage(img10, 100, 750);
		var img13 = new Image();
		if (fire_owned)
		{
			img13.src = "fireballso.png"
		}
		else
		{
			img13.src = "fireballs.png"
		}
		drawRotatedImage(img13, 300, 750);
		var img14 = new Image();
		if (mini_owned)
		{
			img14.src = "minigunso.png"
		}
		else
		{
			img14.src = "miniguns.png"
		}
		drawRotatedImage(img14, 500, 750);
		var img18 = new Image();
		if (mm_owned)
		{
			img18.src = "mmissileso.png"
		}
		else
		{
			img18.src = "mmissiles.png"
		}
		drawRotatedImage(img18, 700, 750);
		var img20 = new Image();
		if (laser_owned)
		{
			img20.src = "laserso.png"
		}
		else
		{
			img20.src = "lasers.png"
		}
		drawRotatedImage(img20, 900, 750);
	}
	else if (shop == 3)
	{
		for (i = 0; i < miniturrets.length; i++)
		{
			miniturrets[i].draw();
		}
		var img16 = new Image();
		img16.src = "miniturret_s.png";
		drawRotatedImage(img16, 100, 750);
		ctx.font = "24px Arial";
		ctx.fillText("$" + 50 * minits, 100, 820);
		var img17 = new Image();
		img17.src = "miniminigun_s.png";
		drawRotatedImage(img17, 300, 750);
		ctx.fillText("$" + 500 * mmguns, 300, 820);
	}
	ctx.font = "32px Arial";
	ctx.fillText("$" + dough, 0, 50);
	if (del != null)
	{
		if (!bullets[del].mini)
		{
			shots--;
		}
		bullets[del] = undefined;
		del = null;
	}
	if (del2 != null)
	{
		enemies[del] = undefined;
		del2 = null;
	}

}, 50);

function getRndNum(min, max)
{
	return (Math.random() * (max - min)) + min;
}

function openShop()
{
	paused = true;
	shop = 1;
}

function closeShop()
{
	paused = false;
	shop = 0;
}

function toggleShop()
{
	if (shop == 0)
	{
		paused = true;
		shop = 1;
	}
	else
	{
		paused = false;
		shop = 0;
	}
}

function startAudioLoop()
{
	if (!musicPlaying)
	{
		setInterval(function ()
		{
			audio_loop++;
			if (audio_loop == 1420)
			{
				var audio = new Audio('Melodic_Chaos.mp3');
				audio.play();
				audio_loop = 0;
			}
		}, 50);
		musicPlaying = true;
	}
}

$("#canvas").click(function (event)
{
	for (i = 0; i < miniturrets.length; i++)
	{
		miniturrets[i].memory = 200;
	}
	clickx = event.offsetX;
	clicky = event.offsetY;
	if (place != 0)
	{
		miniturrets.push(new MiniTurret(clickx, clicky, place));
		place = 0;
	}
	if (event.offsetX >= 1160 && event.offsetX <= 1238 && event.offsetY >= 720 && event.offsetY <= 821)
	{ // click on Shop button
		toggleShop();
	}
	turretFacing = Math.atan2(event.offsetY - 625, event.offsetX - 625);
	if ((((gameTicksSinceLastShot > 24 - upg2 + (upg4 * 6) * 2) || (gameTicksSinceLastShot > 12 - upg2 + (upg4 * 2) && shot != 3)) && gameTicksSinceLastShot > 3 && (shot != 4 || shots < 4 + upg4)) || shot == 5)
	{
		if (shot == 1)
		{
			bullets.push(new Bullet(turretFacing - Math.PI / 2 + (Math.random() - 0.5) / (upg3 + 2), 625, 625, shot, gameTicksSinceLastShot));
		}
		else if (shot != 3 && shot != 5)
		{
			bullets.push(new Bullet(turretFacing - Math.PI / 2 + (Math.random() - 0.5) / (upg3 + 1), 625, 625, shot, gameTicksSinceLastShot));
		}
		else if (shot == 3)
		{
			for (k = 0; k < upg4 + 3; k++)
			{
				bullets.push(new Bullet(turretFacing - Math.PI / 2 + (Math.random() - 0.5) / (upg3 / 2 + 0.5), 625, 625, shot, gameTicksSinceLastShot));
			}
		}
		else
		{
			bullets.push(new Bullet(turretFacing - Math.PI / 2, 625, 625, shot, 1000));
		}
		gameTicksSinceLastShot = 0;
	}
	if (shop == 1)
	{
		if (event.offsetY >= 675 && event.offsetY <= 825 && event.offsetX >= 0 && event.offsetX <= 200)
		{
			shop = 2;
		}
		if (event.offsetY >= 675 && event.offsetY <= 825 && event.offsetX >= 200 && event.offsetX <= 400)
		{
			shop = 3;
		}
		if (event.offsetY >= 675 && event.offsetY <= 825 && event.offsetX >= 400 && event.offsetX <= 600)
		{
			shop = 4;
		}
	}
	else if (shop == 4)
	{
		if (event.offsetY >= 675 && event.offsetY <= 825 && event.offsetX >= 0 && event.offsetX <= 200 && dough > 100 * upgs - 1)
		{
			dough -= 100 * upgs;
			upg1 += 1;
			upgs++;
		}
		if (event.offsetY >= 675 && event.offsetY <= 825 && event.offsetX >= 200 && event.offsetX <= 400 && dough > 100 * upgs - 1)
		{
			dough -= 100 * upgs;
			upg2 += 1;
			upgs++;
		}
		if (event.offsetY >= 675 && event.offsetY <= 825 && event.offsetX >= 400 && event.offsetX <= 600 && dough > 100 * upgs - 1)
		{
			dough -= 100 * upgs;
			upg3 += 1;
			upgs++;
		}
		if (event.offsetY >= 675 && event.offsetY <= 825 && event.offsetX >= 600 && event.offsetX <= 800 && dough > 100 * upgs - 1)
		{
			dough -= 100 * upgs;
			upg4 += 1;
			upgs++;
		}
	}
	else if (shop == 2)
	{
		if (event.offsetY >= 675 && event.offsetY <= 825 && event.offsetX >= 0 && event.offsetX <= 200)
		{
			if (dough > 74 && !pierce_owned)
			{
				dough -= 75;
				pierce_owned = true;
				shot = 1;
			}
			else if (pierce_owned)
			{
				shot = 1;
			}
		}
		if (event.offsetY >= 675 && event.offsetY <= 825 && event.offsetX >= 200 && event.offsetX <= 400)
		{
			if (dough > 249 && !fire_owned)
			{
				dough -= 250;
				fire_owned = true;
				shot = 2;
			}
			else if (fire_owned)
			{
				shot = 2;
			}
		}
		if (event.offsetY >= 675 && event.offsetY <= 825 && event.offsetX >= 400 && event.offsetX <= 600)
		{
			if (dough > 249 && !mini_owned)
			{
				dough -= 250;
				mini_owned = true;
				shot = 3;
			}
			else if (mini_owned)
			{
				shot = 3;
			}
		}
		if (event.offsetY >= 675 && event.offsetY <= 825 && event.offsetX >= 600 && event.offsetX <= 800)
		{
			if (dough > 99 && !mm_owned)
			{
				dough -= 100;
				mm_owned = true;
				shot = 4;
			}
			else if (mm_owned)
			{
				shot = 4;
			}
		}
		if (event.offsetY >= 675 && event.offsetY <= 825 && event.offsetX >= 800 && event.offsetX <= 1000)
		{
			if (dough > 999 && !laser_owned)
			{
				dough -= 1000;
				laser_owned = true;
				shot = 5;
			}
			else if (laser_owned)
			{
				shot = 5;
			}
		}
	}
	else if (shop == 3)
	{
		if (event.offsetY >= 675 && event.offsetY <= 825 && event.offsetX >= 0 && event.offsetX <= 200 && dough > 50 * minits - 1)
		{
			dough -= 50 * minits;
			minits++;
			place = 1;
			alert("Click somewhere to place this Miniturret.");
		}
		if (event.offsetY >= 675 && event.offsetY <= 825 && event.offsetX >= 200 && event.offsetX <= 400 && dough > 500 * mmguns - 1)
		{
			dough -= 500 * mmguns;
			mmguns++;
			place = 2;
			alert("Click somewhere to place this Mini-minigun.");
		}

	}
});

$("body").keydown(function (event)
{
	if (event.keyCode == 32)
	{
		toggleShop();
	}
});

startAudioLoop();